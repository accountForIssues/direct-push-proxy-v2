<?php

declare(strict_types=1);

namespace OCA\SSEPush\Migration;

use Closure;
use OCP\DB\ISchemaWrapper;
use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

/**
 * Auto-generated migration step: Please modify to your needs!
 */
class Version4Date20210518225721 extends SimpleMigrationStep {

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function preSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 * @return null|ISchemaWrapper
	 */
	public function changeSchema(IOutput $output, Closure $schemaClosure, array $options): ?ISchemaWrapper {
		/** @var ISchemaWrapper $schema */
		$schema = $schemaClosure();

		if (!$schema->hasTable('ssepush_tokens')) {
			$table = $schema->createTable('ssepush_tokens');
			$table->addColumn('user_id', 'string', [
				'notnull' => true,
			]);
			$table->addColumn('ext_name', 'string', [
				'notnull' => true,
			]);
			$table->addColumn('token', 'string', [
				'notnull' => true,
			]);
		}

		if (!$schema->hasTable('ssepush_config')) {
			$table = $schema->createTable('ssepush_config');
			$table->addColumn('parameter', 'string', [
				'notnull' => true,
			]);
			$table->addColumn('value', 'string', [
				'notnull' => true,
			]);
		}
		return $schema;
	}

	/**
	 * @param IOutput $output
	 * @param Closure $schemaClosure The `\Closure` returns a `ISchemaWrapper`
	 * @param array $options
	 */
	public function postSchemaChange(IOutput $output, Closure $schemaClosure, array $options): void {
	}
}
