<?php
namespace OCA\SSEPush\Settings;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\Settings\ISettings;
use OCP\IDBConnection;
use OCP\IGroupManager;
use OCP\IUserSession;

class PersonalSettings implements ISettings {

	private $db;
	private $userSession;
	private $groupManager;

	public function __construct(IDBConnection $db, IUserSession $userSession, IGroupManager $groupManager){
		$this->db = $db;
		$this->userSession = $userSession;
		$this->groupManager = $groupManager;
	}

	public function getForm() {
		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('ssepush_tokens')
			->where($query->expr()->eq('user_id', $query->createNamedParameter($this->userSession->getUser()->getUID())));
		$result = $query->execute();
		$items = array();
		while ($row = $result->fetch()){
			$items[] = [
				'ext_name' => $row['ext_name'],
				'token' => $row['token'],
			];
		}
		$result->closeCursor();
		$parameters = array();
		$parameters['parameters'] = $items;
		$parameters['admin'] = $this->groupManager->isAdmin($this->userSession->getUser()->getUID());
		return new TemplateResponse("ssepush", 'personal-settings', $parameters);
	}

	public function getSection() {
		return "ssepush";
	}

	public function getPriority() {
		return 0;
	}
}
