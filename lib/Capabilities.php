<?php
declare(strict_types=1);
namespace OCA\SSEPush;

use OCP\Capabilities\IPublicCapability;

class Capabilities implements IPublicCapability {

	public function getCapabilities() {
		return [
			'ssepush' => [
				'enabled' => true,
			],
		];
	}

}
